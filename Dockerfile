FROM rocker/tidyverse:3.4.1
MAINTAINER Daniel Herman <daniel.herman2@uphs.upenn.edu>

# Create a new user with an expired password
RUN groupadd -g 1004 Rlearners
RUN useradd --create-home --shell /bin/bash -u 1003 -G Rlearners,sudo -p $(openssl passwd -1 PL3@5ecH@nG3) rlearner
RUN passwd -e rlearner
RUN deluser rstudio

# Setup password requirements
RUN apt-get update -qq && \
	apt-get -y install \
		nano \
		libpam-pwquality
RUN sed -i '25s/.*/password\trequisite\t\t\tpam_pwquality.so retry=3 difok=1 minlen=9 ucredit=-1 lcredit=-1 dcredit=-1 ocredit=-1 enforce_for_root/' /etc/pam.d/common-password

# Install R packages needed
RUN install2.r --error \
	--deps FALSE \
	logging \
	cowplot \
	kableExtra \
	class \
	e1071 \
	mcr \
	MuMIn
