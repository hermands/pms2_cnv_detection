=============
PMS2 copy number analysis
=============

SETUP
==========
- Checkout branch research-PMS2cnv

RUN individual analysis
==========
~/repos/targeted_gene_capture_pipeline/new_tgc_bam_analysis.sh <output folder> <branch> <#jobs> run

Example:
~/repos/targeted_gene_capture_pipeline/new_tgc_bam_analysis.sh /mnt/disk3/genetics/archival/tgc_output/160309_HA0303_ColoSeq150-BROv10 research-PMS2cnv 4 run
cat 160309_HA0303_ColoSeq150-BROv10/output/logs/PMS2_DoC  #check log

RUN set
==========
# Choose target runs
target_folders=/mnt/disk3/genetics/archival/tgc_output/*Colo*
#target_folders=`ls -d /mnt/disk1/genetics/2015/15@(0[6789]|1)*Colo*`

# Make a folder for each run with repository and launch PMS2 scons
for A in $target_folders; do cmd="$HOME/repos/targeted_gene_capture_pipeline/new_tgc_bam_analysis.sh $A research-PMS2cnv 10 run"; echo $cmd; $cmd; done

# Check for goal files
for A in $target_folders; do dir=`basename $A`; ls -lh $dir/output/*PMS2_DoC.txt; done

# Check for anything in logs
for A in $target_folders; do dir=`basename $A`; cat $dir/output/logs/PMS2_DoC; done

# Combine results into single TXT file
~/repos/targeted_gene_capture_pipeline/dev/combine_PMS2.R $dir/output/*Combined_PMS2_DoC.txt

# Combine set into archive
tar -czvf PMS2.all.tar.gz $dir/output/*.txt PMS2_all.txt
