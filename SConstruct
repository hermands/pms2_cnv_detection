"""
Genetics NGS pipeline
"""
import os
import sys
import re

from os import path
from ConfigParser import SafeConfigParser
import ConfigParser
from SCons.Script import (Decider, Variables, Alias, Help,
                          AllowSubstExceptions)

# Re-run based on time and MD5
Decider('MD5-timestamp')

##########
# Settings
##########
# declare variables for the environment
vars = Variables(None, ARGUMENTS)
vars.Add(PathVariable('settings', 'configuration file', 'configs/settings.conf'))
vars.Add(PathVariable('data', 'data file', 'configs/data.conf'))
vars.Add(PathVariable('mask', 'mask file', 'configs/mask.conf'))
vars.Add(PathVariable('output', 'Path to output directory',
                      'output', PathVariable.PathIsDirCreate))

# Provides access to options prior to instantiation of env object
# below; it's better to access variables through the env object.
varargs = dict({opt.key: opt.default for opt in vars.options}, **vars.args)

# get default locations of input file for pipeline programs from settings.conf
settings = varargs['settings']
if not path.exists(settings):
    print 'Either create "settings.conf" or use "scons settings=settings-example.conf"'
    sys.exit(1)

# get the absolute paths to each input file from the data.conf
data = varargs['data']
if not path.exists(data):
    print 'Either create "data.conf" or use "scons data=data.conf"'
    sys.exit(1)

# get the settings
_params = SafeConfigParser(allow_no_value=True)
_params.optionxform = str # Make sure keys are case sensitive
_params.read(settings)

# get the bams from the data file
_data = SafeConfigParser()
_data.optionxform = str # Make sure keys are case sensitive
_data.read(data)

# get the masking setup requested in the mask.conf file
if not path.exists(varargs['mask']):
    print 'No masking file found. Running full panel'
    mask = False
else:
    mask = True
    _data.read(varargs['mask'])

##############
# Environment
##############

# Prepare variables and environment
scons_vars = Variables()
for k, v in _params.items('DEFAULT'):
    scons_vars.Add(k, default=v)

venv=_params.get('DEFAULT', 'VIRTUALENV')
print 'looking for virtualenv in "{}"'.format(venv)
if not path.exists(venv):
    sys.exit('No virtualenv is specified.')

# fail if undefined variable used
AllowSubstExceptions()

# PATH and Environment (preference given to local venv executables)
PATH = [
    'bin',
    path.join(venv, 'bin'),
    path.join(venv, 'bin', 'x86_64'),  # required for CREST
    '/usr/local/bin',
    '/usr/bin',
    '/bin'
]

# Prefer the local venv for importing modules
sys.path.insert(0, os.path.join(venv, 'lib', 'python2.7', 'site-packages'))

try:
    from bioscons.slurm import SlurmEnvironment
except ImportError, e:
    print e
    msg = '--> try activating the virtualenv:\nsource {}/bin/activate'
    sys.exit(msg.format(venv))

# Required for BioPerl and CREST
# Breakdancer needs venv/lib/perl5 explicitly
PERL5LIB = [path.join(venv, 'bin'),
            path.join(venv, 'lib'),
            path.join(venv, 'lib', 'perl5'),
            path.join(venv, 'lib', 'perl5', 'x86_64-linux-gnu-thread-multi')]

scons_env = dict(
    os.environ,
    PATH=':'.join(PATH),
    PERL5LIB=':'.join(PERL5LIB),
)
env = SlurmEnvironment(
    ENV=scons_env,
    variables=scons_vars,
    SHELL='bash',
    use_cluster=bool(_params.get('DEFAULT', 'USE_CLUSTER')),
    time=True)
Help(scons_vars.GenerateHelpText(env))

##########
#Run setup
##########
# Run folder is DATE_RUN_PROJECT
seq_run = Dir('.').abspath.split('/')[-1]
try:
    project = Dir('.').abspath.split('_')[2]
#Unless this is a validation run, then use the name of the project
except IndexError:
    project = Dir('.').abspath.split('/')[-1]
print "project:", project
print "seq run:", seq_run

data = {}
for key, bam in _data.items('specimen_data'):
    data[key] = bam.split(',')
    assert(len(data[key]) == 1)
    assert(path.exists(bam))

for k, v in _data.items('assay-version'):
    if not v:
        sys.exit("Please specify the assy and version in the data.conf. examples: OPXv5, BROv7, IMDv1, EPIv1, MRWv3")
    assay=v

HS_METRICS = []  # list of hs_metrics files for PMS2 copy number calling
BAMS = [] # list of bams files for PMS2 copy number calling
Samples = []  # Sample Directories

for pfx in data.keys():
    e = env.Clone()

    #Make the assay specific files available
    try:
        assay_items=dict(_params.items(assay))
    except ConfigParser.NoSectionError:
         sys.exit("Sample {} has assay {}, which is not found in settings.conf. Please make sure assay in data.conf is correct".format(
             pfx, assay))

    #Setup specimen specific variables
    e['specimen'] = pfx
    e['seq_run'] = seq_run
    e['project'] = project
    e['pfxout'] = e.subst('$output/$specimen')
    e['logs'] = path.join(e['pfxout'], 'logs')

    bam = data[pfx]

    # HSMetrics
    hs_metrics, log = e.Command(
        target=['$pfxout/${specimen}.hs_metrics',
                '$logs/HsMetrics'],
        source=[bam,
                '$PICARD_HSMET',
                assay_items['PICARD_BED'],
                '$REF_FASTA'],
        action=('java -Xmx4g -jar ${SOURCES[1]} '
                'BAIT_SET_NAME=${SOURCES[2]} '
                'BAIT_INTERVALS=${SOURCES[2]} '
                'TARGET_INTERVALS=${SOURCES[2]} '
                'VALIDATION_STRINGENCY=SILENT '
                'REFERENCE_SEQUENCE=${SOURCES[3]} '
                'INPUT=${SOURCES[0]} '
                'OUTPUT=${TARGETS[0]} '
                '2> ${TARGETS[1]} '))

    env['PMS2_CNV_BED']=assay_items['PMS2_CNV_BED']

    HS_METRICS.append(hs_metrics) # Add hs_metrics to lsit
    BAMS.append(bam)    # Add bam to list
    Samples.append(e.subst('$pfxout')) # Add sample directories to sample lis
    env['assay']=assay_items['ASSAY']

#Perform run level analysis
run_env = env.Clone()
run_env['project'] = project
run_env['logs'] = run_env.subst('$output/logs')
run_env['seq_run'] = seq_run

### Run steps for PMS copy number calling, only on BRO
if re.search('BRO', assay) or assay == 'TESTDATA':
    # Make BAM-file list
    BAM_list_file = run_env.Command(
        target='$output/bams.list',
        source=BAMS,
        action=('ls $SOURCES > $TARGET'))

    # Run GATK DepthOfCoverage for PMS2
    PMS2_DoC, log = run_env.Command(
        target=['$output/PMS2_DoC.sample_interval_summary',
                '$logs/PMS2_DoC'],
        source=[BAM_list_file,
                '$GATK_JAR',
                '$REF_FASTA',
                ['$PMS2_CNV_BED']],
        action=('java -Xmx4g '
                '-jar ${SOURCES[1]} '
                '--analysis_type DepthOfCoverage '
                '--reference_sequence ${SOURCES[2]} '
                '-L ${SOURCES[3]} '
                '--input_file ${SOURCES[0]} '
                '-o $output/PMS2_DoC '
                '--logging_level ERROR '
                '--log_to_file ${TARGETS[1]} '
                '--omitDepthOutputAtEachBase '
                '--omitLocusTable '
                '--countType COUNT_FRAGMENTS '
                '--includeRefNSites '
                '--minMappingQuality 0 '
                '--minBaseQuality 0'))

    # Make HS-file list
    HS_METRICS_list_file = run_env.Command(
        target='$output/hs_metrics.list',
        source=HS_METRICS,
        action=('ls $SOURCES > $TARGET'))

    # Run R script
    PMS2_CNV_output = run_env.Command(
        target='$output/${project}.Combined_PMS2_DoC.txt',
        source=[PMS2_DoC,
                ['$PMS2_CNV_BED'],
                HS_METRICS_list_file],
        action=('PMS2_process.R '
                '${SOURCES[0]} '
                '${SOURCES[1]} '
                '${SOURCES[2]} '
                '$TARGETS'))
    Alias('PMS2_CNV_output', PMS2_CNV_output)

