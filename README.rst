===============================================
PMS2 copy number variant detection
===============================================

This repository

.. contents:: Table of Contents

Setup R environment
===================
Build docker image::

  cd <repository root dir>
  docker build -t pms2 .

Run docker container::

  docker run -d --rm -p 40021:8787 --storage-opt size=30G --name PMS2 -v ``pwd``:/home/rlearner/ pms2

Change password for user (rlearner)::

  docker exec -it PMS2 /bin/bash
  passwd rlearner
  exit

Generation of target read counts
================================
The ``new_tgc_bam_analysis.sh`` and ``SConstruct`` files are the instructions used to process .bam files to generate _PMS2_ target read counts using GATK DepthOfCoverage and Picard HsMetrics mean target coverage. **Note** that this SConstruct is not automatically functional in this repository, because it does not include the necessary environment configuration.  Rather, it is included as a recipe for how read counts were generated.

See the environmental variables that specify location of installed Picard and GATK in ``configs/settings-example.conf`` and ``dev/`` install scripts for how we have installed these utilities.


Normalization of target read counts
===================================
The last step in the ``SConstruct`` file is to pass the coverage data to ``bin/PMS2_process.R`` to normalize coverage for specified targets::

    PMS2_CNV_output = run_env.Command(
        target='$output/${project}.Combined_PMS2_DoC.txt',
        source=[PMS2_DoC,
                ['$PMS2_CNV_BED'],
                HS_METRICS_list_file],
        action=('PMS2_process.R '
                '${SOURCES[0]} '
                '${SOURCES[1]} '
                '${SOURCES[2]} '
                '$TARGETS'))
    Alias('PMS2_CNV_output', PMS2_CNV_output)

Input requirements can also be checked directly::

  bin/PMS2_process.R --help

Customization of targets
========================
This method should generalize to any set of highly homologous targets. To apply this method to your targets of interest you will need to generate a .bed file matching the formatting of ``data/PMS2.bed`` and customize script ``bin/PMS2_process.R``.

The paired targets need to be specified in the list of ``targets`` in ``bin/PMS2_process.R``. For example, if gene A and B are pairs::

  targets <- list('A_B' = c('A', 'B'))

Targets that are highly homologous need to be named with the same prefix in ``data/PMS2.bed``, such as ``EX12``::

  7       6022350 6022800 EX12_A       0       -
  7       6781139 6781588 EX12_B     0       +

Analysis for manuscript
=======================

The resulting PMS2 output files for each batch were collated using ``dev/combine_PMS2.R`` and then input into excel file ``data/Final data for manuscript_v4.xlsx``. This final dataset was then processed and analyzed using Rmarkdown document ``PMS2_mut_detection.Rmd`` and helper functions in ``PMS2_mut_detection.R``.

License
=======

Copyright (c) 2018 Daniel Herman

Released under the MIT License:

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
