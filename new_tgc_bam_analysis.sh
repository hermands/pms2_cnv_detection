#!/bin/bash

set -e

if [[ -z $1 ]]; then
   echo "usage: $(basename $0) abs-path-to-bams [branch-or-tag] #jobs run"
   exit 1
fi

bams=$1
branch=$2
jobs=$3
run=$4

data_id=$(basename $bams)

outdir=$data_id   # work in subdirectory of launch dir
echo "bams: $bams"
echo "output directory: $outdir"

if [ ! -d $outdir ]; then
    #Clone the repo into the data directory
    GIT_SSH=/mnt/disk2/com/ssh/git_ssh.sh \
	git clone git@bitbucket.org:uwlabmed/targeted_gene_capture_pipeline.git $outdir

    # git clone does not obey umask setting
    chmod g+w $outdir
fi

# Transfer remote data over
if [[ $bams == *":"* ]]; then
    rsync -rvzuP --include='*/' --include='*final.bam' --include='*final.bam.bai' \
	--exclude='*' $bams /mnt/disk4/projects/dan_herman_project/PMS2/tmp/
    bams=`readlink -m /mnt/disk4/projects/dan_herman_project/PMS2/tmp/$outdir`
fi

if [[ ! -d $bams ]]; then
    echo "Error: the directory $bams does not exist"
    exit 1
fi

if [[ -z $jobs ]]; then
    jobs=4
fi

cd $outdir

# if no branch, tag, or commit is specified, check out research-PMS2cnv
if [[ -z $branch ]]; then
    branch=research-PMS2cnv
fi
echo "Using pipeline version $branch"
git checkout $branch

#add the git commit version of the pipeline being run
git_ver="$(git describe)"
sed "s@git_version =@git_version = $git_ver@" configs/settings-example.conf > configs/settings.conf

# capture BAM list
{
    bam_list=`ls -d -1 $bams/BAMS/*final.bam`
} || {
    bam_list=`ls -d -1 $bams/BAM/*final.bam`
} || {
    bam_list=`ls -d -1 $bams/output/*/*final.bam`
} || {
    bam_list=`ls -d -1 $bams/*final.bam`
} || {
    echo "ERROR: could not find bams"
    exit 0
}

echo "[specimen_data]"> configs/data.conf
#Grab only read 1 for parsing the name and absolute path from
for i in $bam_list; do
    #get the basename of abs path $i, set pfx to the ID before .1.
    pfx=$(basename $i)
    #Print this to the data.conf
    echo "$pfx = $i" >> configs/data.conf
done

# Capture assay version
if [ -e $bams/settings_files/data.conf ] && grep --quiet '\[assay\-version\]' $bams/settings_file_data.conf; then
    tail -n 2 $bams/settings_files/data.conf >> configs/data.conf
else
    if [[ $bams =~ .*'_v'[0-9]$ ]]; then  # NOTE: only looking for single digit version
	assay_version=BROv${bams:(-1)}
    elif [[ $bam_list =~ .*'BROv'[0-9].* ]]; then
	tmp=${bam_list##*BROv}
	assay_version=BROv${tmp:0:1}
    elif [[ $bam_list =~ .*'CON'.*'V'[0-9] ]]; then
	tmp=${bam_list##*CON*V}
	assay_version=BROv${tmp:0:1}
    else
	echo "ERROR: could not assign assay version"
	exit 0
    fi
echo -e "[assay-version]\nassay=$assay_version" >> configs/data.conf
fi

echo "data.conf"
cat configs/data.conf

if [[ ! -z $run ]] && [[ "$run" == "run" ]]; then
    echo "RUNNING scons"
    scons -j $jobs
fi

